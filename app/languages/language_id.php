all.lebel.button.save=Simpan
all.lebel.button.upload=Unggah

all.lebel.select=Silahkan pilih

all.label.message.failed_save=Penyimpanan data gagal dilakukan
all.label.message.success_save=Penyimpanan data berhasil dilakukan
all.label.message.failed_delete=Menghapus data gagal dilakukan
all.label.message.success_delete=Menghapus data berhasil dilakukan

index.title=Selamat Datang di KamarDagang

# API
api.message.error.badrequest=Permintaan ditolak, mohon periksa parameter yang Anda berikan.
api.message.error.datanotfound=Data tidak ada.
api.message.notice.teapot=I am teapot!
api.message.info.httpok=OK

login.label.username=E-Mail atau telepon
login.label.password=Kata Sandi
login.label.remember=Ingat saya
login.label.forgot=Lupa Kata Sandi?
login.label.signin=Masuk
login.message.error=Salah e-mail atau kata sandi
login.message.errorstatus=Akun Anda belum diaktifkan
# API
login.message.invalidappkey=Application Key tidak valid, tolong daftarkan aplikasi Anda terlebih dahulu.
login.message.invalidsessionkey=Session Key tidak valid, mohon login kembali.
login.message.userloggedin=Login Sukses
login.message.errorlogout=Maaf, tidak bisa logout saat ini.
login.message.erroralreadylogout=Maaf, session Anda sudah habis, mohon login kembali.
login.message.successlogout=Logout Sukses.

signup.label.signup=Mendaftar
signup.label.cancel=Keluar
signup.label.firstname=Nama depan
signup.label.lastname=Nama belakang
signup.label.email=E-Mail
signup.label.password=Kata sandi
signup.label.confirmpassword=Konfirmasi kata sandi
signup.label.mobilephone=Telepon
signup.label.checkagree=Saya setuju dengan
signup.label.checkterms=Syarat dan Ketentuan

signup.message.error.firstname.required=Masukkan nama depan Anda
signup.message.error.firstname.min=Nama depan Anda harus terdiri dari minimal 2 karakter
signup.message.error.lastname.required=Masukkan nama belakang Anda
signup.message.error.email.required=Masukkan alamat email
signup.message.error.email.notvalid=Masukkan alamat email yang valid
signup.message.error.email.remote=Alamat email sudah digunakan !
signup.message.error.mobile.required=Masukan no telepon
signup.message.error.mobile.remote=No telepon sudah digunakan !
signup.message.error.email_mobile.remote=Alamat Email atau No telepon sudah digunakan !
signup.message.error.password.required=Silahkan memberikan password
signup.message.error.password.min=Password Anda minimal harus 6 karakter
signup.message.error.confirmpassword.required=Silahkan memberikan konfirmasi password
signup.message.error.confirmpassword.min=Konfirmasi Password Anda minimal harus 6 karakter
signup.message.error.confirmpassword.equal=Silahkan masukkan password yang sama seperti di atas
signup.message.error.agree.required=Harap menerima kebijakan kami
# API
signup.message.successregister=Pendaftaran sukses.
signup.message.errorregister=Tidak dapat mendaftar karena Anda sudah memiliki akun dengan email / no handphone tersebut.
signup.message.forgot.invaliduser=Username tidak terdaftar, mohon periksa kembali.
signup.message.forgot.successtoken.checkemail=Kami telah mengirim kode verifikasi ke alamat e-mail Anda, mohon cek kotak masuk Anda.
signup.message.forgot.successtoken.checkmobile=Kami telah mengirim kode verifikasi ke nomor handphone Anda yang terdaftar di sistem kami, mohon cek kotak masuk Anda.
signup.message.forgot.success.checkemail=Kami telah mengirim password yang baru ke alamat e-mail Anda, mohon cek kotak masuk Anda.
signup.message.forgot.success.checkmobile=Kami telah mengirim password yang baru ke nomor handphone Anda yang terdaftar di sistem kami, mohon cek kotak masuk Anda.
signup.message.forgot.error.invalidtokengiven=Token tidak valid, mohon periksa kembali.

regBusiness.title=Pendaftaran Bisnis
regBusiness.subtitle1=Lokasi Bisnis
regBusiness.subtitle2=Informasi Bisnis
regBusiness.subtitle3=Nama Bisnis

regBusiness.label.button.add=Tambah
regBusiness.label.button.upload=Unggah
regBusiness.label.button.submit=Buat Bisnis
regBusiness.label.button.uploaddoc=Unggah File Dokumen/Gambar

regBusiness.label.youare=Mendaftarkan Bisnis Sebagai
regBusiness.label.rdOwner=Pemilik
regBusiness.label.rdAdmin=Admin
regBusiness.label.typeBusiness=Tipe Bisnis
regBusiness.label.rdOrg=Organisasi
regBusiness.label.rdBusiness=Bisnis
regBusiness.label.ownername=Nama Pemilik
regBusiness.label.owneremail=Email Pemilik
regBusiness.label.ownermobile=Telepon Pemilik
regBusiness.label.legalbusinessname=Nama Bisnis Legal
regBusiness.label.tradebusinessname=Nama Bisnis Dagang
regBusiness.label.titlebusinessname=Nama Judul Bisnis
regBusiness.label.country=Negara
regBusiness.label.province=Provinsi
regBusiness.label.regency=Kota/Kabupaten
regBusiness.label.subdistrict=Kecamatan
regBusiness.label.address=Alamat
regBusiness.label.phone=Telepon
regBusiness.label.fax=Fax
regBusiness.label.email=E-Mail
regBusiness.label.urlweb= URL Web
regBusiness.label.legaldocument=Dokumen Bisnis
regBusiness.label.agregatorname=Nama Pembuat Dokumen
regBusiness.label.agregatorphone=Telepon Agregator
regBusiness.label.agregatoremail=E-Mail Agregator
regBusiness.label.agregatorproduct=Pilih Produk
regBusiness.label.agregatorDocName=Nama Dokument
regBusiness.label.agregatorDocNo=Nomor ID Dokumen
regBusiness.label.documentStartDate=Dibuat Pada
regBusiness.label.documentEndDate=Berlaku Sampai
regBusiness.label.type=Tipe

generic.label.error.not_post_request=Akses proses anda tidak valid
generic.label.error.failed_save=Penyimpanan data gagal dilakukan
generic.label.error.success_save=Penyimpanan data berhasil dilakukan
generic.label.error.already_exist=Data sudah ada!

editMember.title.general=Informasi Umum
editMember.title.modalpassword=Ubah Password Anda
editMember.title.modalpicture=Ganti Foto Profil

editMember.label.button.changepricture=Ubah Gambar
editMember.label.button.changepassword=Ganti Kata Sandi
editMember.label.button.savechange=Simpan Perubahan
editMember.label.button.zoomin=Diperbesar
editMember.label.button.zoomout=Diperkecil
editMember.label.button.setpic=Set Foto Profil

editMember.label.firshname=Nama Depan
editMember.label.lastname=Nama Belakang
editMember.label.identitycard=Kartu identitas
editMember.label.email=E-mail
editMember.label.mobile=Telepon

editMember.modal.label.oldpass=Kata Kunci Lama
editMember.modal.label.newpass=kata kunci Baru
editMember.modal.label.confpass=Konfirmasi Kata Kunci Baru
