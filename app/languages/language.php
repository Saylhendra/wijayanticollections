all.lebel.button.save=Save
all.lebel.button.upload=Upload

all.lebel.select=Please select

all.label.message.failed_save=Failed to save data
all.label.message.success_save=Succeed to save data
all.label.message.failed_delete=Failed to delete data
all.label.message.success_delete=Succeed to delete data

index.title=Welcome to KamarDagang

# API
api.message.error.badrequest=Bad Request, please check your parameter.
api.message.error.datanotfound=Data Not Found.
api.message.notice.teapot=I am teapot!
api.message.info.httpok=OK

login.label.username=E-Mail or Mobile Phone
login.label.password=Password
login.label.remember=Remember me
login.label.forgot=Forgot your password ?
login.label.signin=Sign in
login.message.error=Wrong username or password
login.message.errorstatus=Your account has not been activated
# API
login.message.invalidappkey=Invalid Application Key, please register your application first.
login.message.invalidsessionkey=Invalid Session Key, please relogin.
login.message.userloggedin=Login Successfully.
login.message.errorlogout=Can not logout at the moment.
login.message.erroralreadylogout=Your session is expired, please re-login.
login.message.successlogout=Logout Successfully.

signup.label.signup=Sign Up
signup.label.cancel=Cancel
signup.label.firstname=First Name
signup.label.lastname=Last Name
signup.label.email=E-Mail
signup.label.password=Password
signup.label.confirmpassword=Confirm Password
signup.label.mobilephone=Mobile Phone
signup.label.checkagree=I agree with the
signup.label.checkterms=Terms & Conditions

signup.message.error.firstname.required=Please enter a first name
signup.message.error.firstname.min=Your first name must consist of at least 2 characters
signup.message.error.lastname.required=Please enter your last name
signup.message.error.email.required=Please enter a email address
signup.message.error.email.notvalid=Please enter a valid email address
signup.message.error.email.remote=Email address already in use!
signup.message.error.mobile.required=Please enter a mobile phone
signup.message.error.mobile.remote=Mobile Phone already in use!
signup.message.error.email_mobile.remote=Email Address or Phone number already in use!
signup.message.error.password.required=Please provide a password
signup.message.error.password.min=Your password must be at least 6 characters long
signup.message.error.confirmpassword.required=Please provide a confirm password
signup.message.error.confirmpassword.min=Your password must be at least 6 characters long
signup.message.error.confirmpassword.equal=Please enter the same password as above
signup.message.error.agree.required=Please accept our policy
# API
signup.message.successregister=Registration successfully.
signup.message.errorregister=Can not register at the moment, because this email or phone number already registered.
signup.message.forgot.invaliduser=Username is invalid, please check again.
signup.message.forgot.successtoken.checkemail=We have sent you email with verification code, please check your inbox.
signup.message.forgot.successtoken.checkmobile=We have sent you verification code to your registered mobile number, please check your inbox.
signup.message.forgot.success.checkemail=We have sent you email with new password, please check your inbox.
signup.message.forgot.success.checkmobile=We have sent you new password to your registered mobile number, please check your inbox.
signup.message.forgot.error.invalidtokengiven=Invalid token, please check again.

regBusiness.title=Entity Registration
regBusiness.subtitle1=Entity Location
regBusiness.subtitle2=Entity Information
regBusiness.subtitle3=Entity Name
regBusiness.subtitle4=Entity Document

regBusiness.label.button.add=Add
regBusiness.label.button.upload=Upload
regBusiness.label.button.submit=Create Entity
regBusiness.label.button.uploaddoc=Upload Document File/Picture

regBusiness.label.youare=Registering Entity As
regBusiness.label.rdOwner=Owner
regBusiness.label.rdPersonel=Personnel
regBusiness.label.typeBusiness=Entity Type
regBusiness.label.rdOrg=Organisation
regBusiness.label.rdBusiness=Business
regBusiness.label.ownername=Owner Name
regBusiness.label.owneremail=Owner Email
regBusiness.label.ownermobile=Owner Mobile
regBusiness.label.ownermobile=Owner Mobile
regBusiness.label.legalbusinessname=Legal Entity Name (official name)
regBusiness.label.tradebusinessname=Trade Entity Name (alias or popular name)
regBusiness.label.titlebusinessname=Entity Title Name
regBusiness.label.country=Country
regBusiness.label.province=Province
regBusiness.label.regency=Regency
regBusiness.label.subdistrict=Sub-district
regBusiness.label.address=Address
regBusiness.label.phone=Phone
regBusiness.label.fax=Fax
regBusiness.label.email=E-Mail
regBusiness.label.urlweb= Web URL
regBusiness.label.legaldocument=Entity Document
regBusiness.label.agregatorname=Agregator Name
regBusiness.label.agregatorphone=Agregator Phone
regBusiness.label.agregatoremail=Agregator E-Mail
regBusiness.label.agregatorproduct=Enter/Select Product
regBusiness.label.agregatorDocName=Document Name
regBusiness.label.agregatorDocNo=Document ID Number
regBusiness.label.documentStartDate=Created On
regBusiness.label.documentEndDate=Valid Until
regBusiness.label.type=Type

generic.label.error.not_post_request=Your process access is not valid!
generic.label.error.failed_save=Failed to save data
generic.label.error.success_save=Succeed to save data
generic.label.error.already_exist=Data already exist!


editMember.title.general=General Information
editMember.title.modalpassword=Change Your Password
editMember.title.modalpicture=Change Profile Picture

editMember.label.button.changepricture=Change Picture
editMember.label.button.changepassword=Change Password
editMember.label.button.savechange=Save Change
editMember.label.button.zoomin=Zoom In
editMember.label.button.zoomout=Zoom Out
editMember.label.button.setpic=Set Profile Picture

editMember.label.firshname=First Name
editMember.label.lastname=Last Name
editMember.label.identitycard=Identity Card
editMember.label.email=E-mail
editMember.label.mobile=Mobile Phone

editMember.modal.label.oldpass=Old Password
editMember.modal.label.newpass=New Password
editMember.modal.label.confpass=Confirm New Password

