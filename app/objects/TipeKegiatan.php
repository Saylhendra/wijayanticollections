<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class TipeKegiatan
{
	//column type news
    public static $HALAQOH_PEKANAN = 1;
    public static $RIHLA = 2;
    public static $MABIT = 3;
    public static $RIYADOH = 4;
    public static $JAULAH = 5;

    public static function getString($type){
        //$type = intval($type."");
        switch($type){
            case self::$HALAQOH_PEKANAN  : return "HALAQOH";
            case self::$RIHLA  : return "AR RIHLA";
            case self::$MABIT  : return "MABIT";
            case self::$RIYADOH  : return "AR RIYADHOH";
            default : return "JAULAH";
        }
    }

    public static function getList(){
        for($i = 1; $i < 6; $i++){
            $nama = TipeKegiatan::getString($i);
            $hasil[] = $i."-".$nama;
        }
        return $hasil;
    }
}