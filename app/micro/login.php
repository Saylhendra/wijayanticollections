<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Display `OK` message
 *
 * @return json string `OK`
 */
$app->get('/api', function () {
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    $data = array("message" => T::message("api.message.info.httpok"));

    echo json_encode($data);
});

/**
 * Proses permintaan dari app client untuk services mobile dengan memanggil \app\services\MemberService::loginMemberApp
 * @param string username Username atau no handphone pengguna
 * @param string password Password dari pengguna
 * @param string device_id Untuk app client android, yang diminta adalah GCM_REG_ID
 * @param string os_type OS Client, misal: Android 4.1.2, iOS 7.1, dll
 * @param string app_key App Key yang terdaftar. Lihat table `kdg_app`
 *
 * @return string JSON format sesuai dengan spesifikasi berikut:
 * -> Jika username / no handphone dan password salah:
 *    STATUS 401 Authorization Required
 *    {
 *       "isSuccess": false,
 *       "message": "Wrong email or password",
 *       "data": null
 *    }
 * -> Jika username / no handphone dan password benar:
 *    STATUS 200 OK
 *    {
 *           "isSuccess": true,
 *           "message": null,
 *           "data": {
 *               "id": "14239731314261",
 *               "first_name": "baskoro",
 *               "last_name": "adi putro",
 *               "email": "dimaskoro88@gmail.com",
 *               "mobile": "087871875353",
 *               "status_active": "1",
 *               "user_photo": "http:\/\/kamardagang.com\/public\/uploads\/pictures\/2015_05_07\/14309834891189_medium.png",
 *               "business_list": [
 *                   {
 *                       "id": "gxauAbfJeBDC550014fa89cc3550014fa89cfe550014fa89d382rVm2",
 *                       "id_business": "gxauAbfJeBDC550014fa89cc3550014fa89cfe550014fa89d382rVm2",
 *                       "id_member": "14239731314261",
 *                       "status_active": "1",
 *                       "trade_business_name": "BIT ",
 *                       "title_business_name": " ",
 *                       "subtitle": " subdiary ",
 *                       "divtitle": " division Information data huhu",
 *                       "path_medium": null
 *                   },
 *                   { ... }, 
 *                   { ... }, 
 *                ]
 *           }
 *      }
 */
$app->post('/api/login', function () {
    global $request;
    $membername = $request->getPost('username');
    $password = $request->getPost('password');
    $device_id = $request->getPost('device_id');
    $os_type = $request->getPost('os_type');
    $app_key = $request->getPost('app_key');

    $result = MemberService::loginMemberApp( $membername, $password, $device_id, $os_type, $app_key );
    header('Content-Type: application/json');
    if ($result->isSuccess === FALSE)
    {
        header('HTTP/1.0 401 Authorization Required');
    }

    echo json_encode($result);
});

/**
 * Proses permintaan dari client app untuk menghapus session dengan memanggil \app\services\MemberService::logoutMemberApp
 * @param string session_key stored session key yang didapat ketika login
 * @param string id_member id member/session yang didapat ketika login
 * @param string app_key registered app key
 *
 * @return string JSON format
 * -> Sukses Logout dan menghapus session key
 *    STATUS 200 OK
 *    {
 *       "isSuccess": true,
 *       "message": "Logout Successfully.",
 *       "data": null
 *    }
 * -> Jika session sudah expired
 *    STATUS 401 Authorization Required
 *    {
 *       "isSuccess": false,
 *       "message": "Your session is expired, please re-login.",
 *       "data": null
 *    }
 * -> Jika app key tidak valid
 *    STATUS 401 Authorization Required
 *    {
 *       "isSuccess": false,
 *       "message": "Invalid Application Key, please register your application first.",
 *       "data": null
 *    }
 */
$app->post('/api/logout', function () {
    global $request;
    $session_key = $request->getPost('session_key');
    $id_member = $request->getPost('id_member');
    $app_key = $request->getPost('app_key');

    $result = MemberService::logoutMemberApp( $session_key, $id_member, $app_key );
    header('Content-Type: application/json');
    if ($result->isSuccess === FALSE)
    {
        header('HTTP/1.0 401 Authorization Required');
    }

    echo json_encode($result);
});
