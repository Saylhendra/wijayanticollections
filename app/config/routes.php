<?php

$router = new Phalcon\Mvc\Router(FALSE);

$router->add('/:controller/:action/:params', array(
	//'namespace' => 'MyApp\Controllers',
	'controller' => 1,
	'action' => 2,
	'params' => 3,
));

$router->add('/:controller', array(
	//'namespace' => 'MyApp\Controllers',
	'controller' => 1
));

/*/////////////////////////////////////////*/
$router->add('/admin/:controller/:action/:params', array(
    'namespace' => 'AdminModul',
    'controller' => 1,
    'action' => 2,
    'params' => 3,
));
$router->add('/admin/:controller/:action', array(
    'namespace' => 'AdminModul',
    'controller' => 1,
    'action' => 2,
));

$router->add('/admin/:controller', array(
    'namespace' => 'AdminModul',
    'controller' => 1
));

/*/////////////////////////////////////////*/
$router->add('/front/:controller/:action/:params', array(
	'namespace' => 'FrontModul',
	'controller' => 1,
	'action' => 2,
	'params' => 3,
));
$router->add('/front/:controller/:action', array(
	'namespace' => 'FrontModul',
	'controller' => 1,
	'action' => 2,
));

$router->add('/front/:controller', array(
	'namespace' => 'FrontModul',
	'controller' => 1
));


return $router;