<?php
$GLOBALS[ 'DATABASE_APP' ] = array(
	'adapter' => 'Mysql',
	'host' => 'wijayanticollections.com',
	//'host' => '127.0.0.1',
	'username' => 'wijayant_admin1',
	'password' => 'p4ssw0rd',
	'dbname' => 'wijayant_tshirt',

);
$GLOBALS[ 'APPLICATION' ] = array(
	'controllersDir' => __DIR__ . '/../../app/controllers/',
	'modelsDir' => __DIR__ . '/../../app/models/',
	'viewsDir' => __DIR__ . '/../../app/views/',
	'pluginsDir' => __DIR__ . '/../../app/plugins/',
	'libraryDir' => __DIR__ . '/../../app/library/',
	'languagesDir' => __DIR__ . '/../../app/languages/',
	'servicesDir' => __DIR__ . '/../../app/services/',
	'objectsDir' => __DIR__ . '/../../app/objects/',
	'cacheDir' => __DIR__ . '/../../app/cache/',
	'baseUri' => '/wijayanticollections/',
	'debugSql' => FALSE,
	'encryptJs' => FALSE
);

return new \Phalcon\Config(array(
	'database' => $GLOBALS[ 'DATABASE_APP' ],
	'application' => $GLOBALS[ 'APPLICATION' ]
));
