<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerNamespaces(array(
	'ApplicationApi' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "api/",
	'FrontModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "front/",
    'AdminModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "admin/",
    'UserModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "user/"
));
$loader->registerDirs(array(
		$GLOBALS[ 'APPLICATION' ][ 'controllersDir' ],
		$GLOBALS[ 'APPLICATION' ][ 'modelsDir' ],
		$GLOBALS[ 'APPLICATION' ][ 'libraryDir' ],
		$GLOBALS[ 'APPLICATION' ][ 'servicesDir' ],
		$GLOBALS[ 'APPLICATION' ][ 'objectsDir' ]
		/*
			$config->application->controllersDir,
			$config->application->modelsDir,
			$config->application->libraryDir,
			$config->application->servicesDir,
			$config->application->objectsDir
		*/
	));
/*
$loader->registerClasses(
    array(
        "I18nLibrary"         => $config->application->libraryDir."/I18nLibrary.php",
        "T"         => $config->application->libraryDir."/T.php"
    )
);
*/
$loader->register();

//$test = I18nLibrary::getPhrase("app.title");