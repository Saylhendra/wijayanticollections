<?php
namespace AdminModul;

class HomeController extends \ControllerBase
{

    private $URL = "home";

    public function indexAction()
    {
        $this->view->partial('admin/'.$this->URL.'/index');
    }

}

