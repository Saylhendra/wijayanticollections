<?php

class IndexController extends ControllerBase
{

    private $URL = "page_signin";

    public function indexAction()
    {
        $this->view->partial('front/'.$this->URL.'/index');
    }

}

