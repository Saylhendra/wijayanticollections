<?php
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class PartialController extends ControllerBase
{
	public function indexAction()
	{
		$path = trim(McryptLibrary::decryptString(str_replace(" ", "+", urldecode($this->request->getQuery('path')))));
		/*
        $path = str_replace("\\", "/", dirname(__FILE__)) . "/../views/" . $path . ".phtml";
		$contents = file_get_contents($path);
		$enc = $contents;
		eval("?>" . $enc);
		*/
        $this->view->partial("/".$path);
	}
}